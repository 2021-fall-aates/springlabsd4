package com.oreillyauto.dao;

import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.EmployeeRepositoryCustom;
import com.oreillyauto.domain.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>, EmployeeRepositoryCustom {
	// Find/Get Employee List
	List<Employee> findByLastNameAndFirstName(String lastName, String firstName);
	List<Employee> getByLastNameAndFirstName(String lastName, String firstName);
	
	// Find/Get Employee By ID
	Optional<Employee> findById(Integer id);
	Optional<Employee> getById(Integer id);
	
	// Count Employees
	Long countByLastName(String lastName);
	
	// ReadBy
	List<Employee> readByLastNameAndFirstName(String lastName, String firstName);
	
	// Query
	List<Employee> queryByLastNameAndFirstName(String lastName, String firstName);
	
	// Before
	List<Employee> findByStartDateBefore(Date date); // java.sql.Date
	
	// Like
	List<Employee> findByLastNameLike(String like);
	
	// IN
	List<Employee> findByFirstNameIn(Collection<String> employeeList);
	
}
