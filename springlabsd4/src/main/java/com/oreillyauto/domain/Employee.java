package com.oreillyauto.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "EMPLOYEES")
@Entity
public class Employee implements Serializable {

	private static final long serialVersionUID = -7834370657927713939L;

	public Employee() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "INTEGER")
	private Integer id;

	@Column(name = "FIRST_NAME", columnDefinition = "VARCHAR(64)")
	private String firstName;

	@Column(name = "LAST_NAME", columnDefinition = "VARCHAR(64)")
	private String lastName;

	@Column(name = "AGE", columnDefinition = "INTEGER")
	private Integer age;

	@Column(name = "START_DATE", columnDefinition = "DATE")
	private Date startDate;

	public Integer getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age
				+ ", startDate=" + startDate + "]";
	}
}
